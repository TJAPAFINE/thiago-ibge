import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Municipio } from './entity/municipio';

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {

  constructor(private http:HttpClient) { }

  getMunicipios(id:number):Observable<Municipio[]> {
    return this.http.get<Municipio[]>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${id}/municipios`);
  }
}
