import { Component, OnInit } from '@angular/core';
import { EstadoService } from '../estado.service';
import { Estado } from '../entity/estado';

@Component({
  selector: 'app-estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.css'],
  providers:[EstadoService]
})
export class EstadoComponent implements OnInit {

  public estados:Estado[];
  constructor(private service:EstadoService) { }

  ngOnInit() {
    this.getEstados();
  }

  getEstados() {
    this.service.getEstados().subscribe(
      response => this.estados = response
    );
  }

}
