import { Component, OnInit, Input } from '@angular/core';
import { MunicipioService } from '../municipio.service';
import { ActivatedRoute } from '@angular/router';
import { Municipio } from '../entity/municipio';

@Component({
  selector: 'app-municipio',
  templateUrl: './municipio.component.html',
  styleUrls: ['./municipio.component.css']
})
export class MunicipioComponent implements OnInit {

  public municipios:Municipio[];

  constructor(
    private service:MunicipioService,
    private route:ActivatedRoute
  ) { }

  ngOnInit() {
    let id:number = +this.route.snapshot.paramMap.get('id');
    this.getMunicipios(id);
  }

  getMunicipios(idEstado:number) {
    this.service.getMunicipios(idEstado).subscribe(
      response => this.municipios = response
    )
  }

}
