import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EstadoComponent } from './estado/estado.component';
import { MunicipioComponent } from './municipio/municipio.component';


const routes: Routes = [
  { path : 'estado', component : EstadoComponent},
  { path : 'estado/:id', component : MunicipioComponent}
]

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
  declarations: []
})
export class AppRoutingModule { 
  
}
